#include "mainwindow.h"

#include "./ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent), ui(new Ui::MainWindow) {
  ui->setupUi(this);
}

MainWindow::~MainWindow() { delete ui; }

void MainWindow::searchScanners() {}

void MainWindow::addScannertoTable() {}

void MainWindow::writeLogLife() {}

void MainWindow::stateOfTheShutters() {}

void MainWindow::sendCommandForTheShutters() {}

void MainWindow::runTheTest() {}
